/////////////////////////////////////
// Building in Decentraland course
// Lesson 9 scene to fix
// 
// this scene started out as a DCL 2.2.6, SDK 5.0.0 "dcl init" scene, as follows:
/*
npm rm -g decentraland
npm i -g decentraland@2.2.5
md Lesson9a-SDK5-FixMe
cd Lesson9a-SDK5-FixMe
dcl init
npm rm decentraland-ecs
npm i decentraland-ecs@5.0.0
// imported an SDK5 version of spawnerFunctions.ts module
// got busy reworking the game.ts file to have the basis for the Lesson assignment
*/

import {spawnPlaneX, spawnBoxX, spawnGltfX} from './modules/spawnerFunctions'
import { GroundCover } from './modules/GroundCover';




const groundMaterial = new Material()
//groundMaterial.albedoColor = new Color3(0.1, 0.4, 0)
groundMaterial.albedoTexture = new Texture("materials/Tileable-Textures/grassy-512-1-0.png")
let ground = new GroundCover(0,0, 16, 0.01, 16, groundMaterial, true)

const box = spawnBoxX(5,0.5,5, 0,0,0,  1,1,1)
const boxMaterial = new Material()
boxMaterial.albedoColor = new Color3(0,0.2,0)
boxMaterial.metallic = 1
boxMaterial.roughness = 0
box.addComponent(boxMaterial)

const cube = spawnGltfX(new GLTFShape("models/Lesson9Cube/Lesson9Cube.glb"), 5,0,6, 0,0,0, 1,1,1)

const grassTuft = spawnGltfX(new GLTFShape("models/GrassTuft/GrassTuft.gltf"), 10,0,2.5, 0,0,0,  1,1,1)
const grassTuft2 = spawnGltfX(new GLTFShape("models/GrassTuft/GrassTuft.gltf"), 3,0,3, 0,0,0,  1,1,1)

const tree1 = spawnGltfX(new GLTFShape("models/Tree/Tree.gltf"), 7,0,5, 0,0,0,  0.3,0.3,0.3)
// const tree2 = spawnGltfX(new GLTFShape("models/Tree/Tree.gltf"), 7,0,3, 0,0,0,  0.3,0.3,0.3)
// const tree3 = spawnGltfX(new GLTFShape("models/Tree/Tree.gltf"), 7,0,3, 0,0,0,  0.3,0.3,0.3)

const firTree = spawnGltfX(new GLTFShape("models/TreeA_Fir/TreeA_Optimised_28_June_2018_A01.babylon.gltf"), 7,0,11, 0,0,0,  .10,.10,.10)

const bird = spawnGltfX(new GLTFShape("models/Sparrow/Sparrow-burung-pipit.gltf"), 5,1,2, 0,0,0, 0.05, 0.05, 0.05)


// Create animator component
let animator = new Animator()

// Add animator component to the entity
bird.addComponent(animator)

// Instance animation clip object
const clipFly = new AnimationState("fly")

clipFly.speed = 3

// Add animation clip to Animator component
animator.addClip(clipFly)

clipFly.play()